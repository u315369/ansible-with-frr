#!/usr/bin/python

ANSIBLE_METADATA = {
    "metadata_version": "1.2",
    "status": ["preview"],
    "supported_by": "community",
}


from ansible.module_utils.basic import AnsibleModule
from ansible.errors import AnsibleError
from gns3fy import Gns3Connector, Project, Node, Link
from ansible.module_utils.frrMods import (
    gns3_srvr_ver,
    gns3_node_inv,
    gns3_boot_status,
    gns3_telnet,
    gns3_node_login,
    gns3_frr_system,
    check_cmd_params,
)


def main():
    """DOCTSTRING."""
    my_parms = {
        "url": {"required": True, "type": "str"},
        "project_name": {"required": True, "type": "str"},
        "node_name": {"type": "str", "default": ""},
        "commands": {"type": "list", "default": ""},
        "remote_dev": {"type": "str", "default": ""},
        "remote_prt": {"type": "str", "default": ""},
        "user": {"type": "str", "default": None},
        "password": {"type": "str", "default": None, "no_log": True},
    }

    module = AnsibleModule(argument_spec=my_parms)
    srvr_url = module.params['url']
    proj_nam = module.params['project_name']
    node_name = module.params['node_name']
    remote_dev = module.params['remote_dev']
    remote_prt = module.params['remote_prt']
    user = module.params['user']
    password = module.params['password']

    # seed the results dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    results = dict(changed=False, message='')

    # define server object to establish the connection
    gc = Gns3Connector(url=srvr_url)

    # obtain gns3 server version
    gns3_srvr_ver(gc)

    # obtain node inventory from Project
    invtry, lab = gns3_node_inv(proj_nam, gc)

    # open the Project/lab
    if 'opened' not in lab.status.lower():
        lab.open()

    # start up Project and Nodes
    end_device = lab.get_node(node_name)
    if 'stopped' in end_device.status.lower():
        end_device.start()
        changed = starting = True
        status = f"{node_name} is now {end_device.status}"
    else:
        changed = starting = False
        status = f"{node_name} is {end_device.status}"

    results.update(changed=changed, status=status)

    # ================================================================

    verified, commands = check_cmd_params(module)
    if verified:
        # fetch required parameters for commands
        remote_dev = module.params['remote_dev']
        remote_prt = module.params['remote_prt']
        user = module.params['user']
        password = module.params['password']

        # telnet to device
        msg, tn = gns3_telnet(remote_dev, remote_prt)
        results.update(message=msg, telnet=[msg, remote_prt])
    else:
        results.update(cmds=None)
        module.exit_json(**results)

    if not tn:
        module.fail_json(msg=f"Failed to access device via telnet")

    # check if nodes are booted
    boot_status = gns3_boot_status(starting, tn)
    results.update(booted=boot_status)

    # log into device, if node is booted
    if boot_status[0]:
        chg, msg = gns3_node_login(tn, user, password)
        results.update(changed=chg, access=msg)

        # configure device
        chg, msg = gns3_frr_system(tn, commands)
        results.update(changed=chg, confmsg=msg)

    module.exit_json(**results)


if __name__ == "__main__":
    main()
