#!/usr/bin/python

ANSIBLE_METADATA = {
    "metadata_version": "1.2",
    "status": ["preview"],
    "supported_by": "community",
}


from ansible.module_utils.basic import AnsibleModule
from ansible.errors import AnsibleError
from gns3fy import Gns3Connector, Project, Node, Link
from ansible.module_utils.myNetMods import get_mode, get_in, get_in_confmode
from ansible.module_utils.frrMods import (
    gns3_srvr_ver,
    gns3_node_inv,
    gns3_boot_status,
    gns3_telnet,
    gns3_node_login,
    gns3_frr_system,
    check_cmd_params,
)


def main():
    """DOCTSTRING."""
    my_params = {
        "url": {"required": True, "type": "str"},
        "project_name": {"required": True, "type": "str"},
        "node_name": {"type": "str", "default": ""},
    }

    module = AnsibleModule(argument_spec=my_params)
    srvr_url = module.params['url']
    proj_nam = module.params['project_name']
    node_name = module.params['node_name']
    changed = False
    msg = ""

    # seed the results dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    results = dict(changed=changed, message=msg)

    # define server object to establish the connection
    gc = Gns3Connector(url=srvr_url)

    # obtain gns3 server version
    gns3_srvr_ver(gc)

    # obtain node inventory from Project
    invtry, lab = gns3_node_inv(proj_nam, gc)
    results.update(node_inv=invtry)

    # open the Project/lab
    if 'opened' not in lab.status.lower():
        lab.open()

    # start up Project Nodes
    start_status = ""
    for end_device, val in invtry.items():
        # obtain end-nodes package set
        gns3fy_node_set = lab.get_node(end_device)

        if end_device == node_name:
            if 'stopped' in gns3fy_node_set.status.lower():
                gns3fy_node_set.start()
                changed = True
                start_status = f"{end_device} just {gns3fy_node_set.status}"
            else:
                changed = False
                start_status = f"{end_device} already {gns3fy_node_set.status}"

    results.update(changed=changed, start_status=start_status)

    module.exit_json(**results)


if __name__ == "__main__":
    main()
