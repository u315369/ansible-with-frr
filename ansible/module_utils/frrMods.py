
from ansible.module_utils.myNetMods import get_mode, get_in, get_in_confmode
from ansible.errors import AnsibleError
from gns3fy import Gns3Connector, Project, Node, Link
import telnetlib
import time


def gns3_srvr_ver(srvr_obj):
    return srvr_obj.get_version()


def gns3_node_inv(lab_name, server):
    # fetch the project from the server
    lab = Project(name=lab_name, connector=server)
    lab.get()
    _dict = {}

    '''
    fetch only relevent data from NODE options incl:
    "console_port": 5002,
    "console_type": "telnet",
    "name": "FRR-2",
    "server": "192.168.0.21",
    "template": null,
    "type": "qemu"
    '''
    for node, val in lab.nodes_inventory().items():
        node_name = lab.nodes_inventory()[node]['name']
        node_port = lab.nodes_inventory()[node]['console_port']
        node_type = lab.nodes_inventory()[node]['type']
        node_srvr = lab.nodes_inventory()[node]['server']
        _dict[node_name] = {
            'node_name': node_name,
            'node_port': node_port,
            'node_type': node_type,
            'node_srvr': node_srvr,
        }

    # return node info in dictionary format
    return (_dict, lab)


def gns3_telnet(gns3srv, node_port):
    """DOCTSTRING."""
    host = gns3srv
    port = node_port

    try:
        tn = telnetlib.Telnet(host, port, timeout=2)
    except Exception as e:
        msg = f"Telnet failed to {host} on port {port}, {e}"
        return (msg, None)
    return ('OK', tn)


def _frr_access(tnObj, confmode=False):
    """Provide access to config mode.

    Internal function to ensure FRR is at enable prompt.
    Also, the function will enter into configuration mode
    if set to True.
    """
    # verify access was granted
    _mode = get_mode(tnObj)[0]
    _cfgmode = ''
    if 'priv' in _mode.lower() and confmode:
        # enter config-mode
        _cfgmode = True if get_in_confmode(tnObj) else False
    elif 'config' in _mode.lower() and confmode:
        _cfgmode = True
    return True if _cfgmode else False


def gns3_frr_interface(tnObj, intf, ipv4):
    """DOCTSTRING."""
    # verify and clean command
    if intf == '' or ipv4 == '':
        return (False, 'No valid command provided')
    en_prompt = "(config-if)#"

    if _frr_access(tnObj, confmode=True):
        # interface configuration
        conf_intf = f"interface {intf.strip()}"
        conf_ipv4 = f"ip address {ipv4.strip()}"
        tnObj.write(conf_intf.encode() + '\n\n'.encode())
        tnObj.read_until(en_prompt.encode(), timeout=5)
        tnObj.write(conf_ipv4.encode() + '\n\n'.encode())
        _r = tnObj.read_until(en_prompt.encode(), timeout=5)
        if "% Unknown" not in _r.decode():
            # exit config mode
            tnObj.write('end'.encode() + '\n\n'.encode())
            tnObj.read_until(en_prompt.encode(), timeout=5)
            chg = True
            msg = f"Interface configuration applied - {intf}"
        else:
            chg = False
            msg = f'Device returned: unknown command'
    else:
        chg = False
        msg = "ERROR: Could not configure device"
    return (chg, msg)


def gns3_frr_system(tnObj, commands, timeout=3):
    """DOCTSTRING."""
    # verify and clean command
    if not commands:
        return (False, 'No valid command provided')

    en_prompt = "(config)#"
    failed = False
    failed_cmds = []

    # send commands
    if _frr_access(tnObj, confmode=True):
        for cmd in commands:
            tnObj.write(cmd.encode() + '\n'.encode())
            _r = tnObj.read_until(en_prompt.encode(), timeout=timeout)

            # stop loop on any command error
            if "% Unknown" in _r.decode() or "% Can't" in _r.decode():
                failed = True
                failed_cmds += [cmd]

        # exit config mode
        tnObj.write('end'.encode() + '\n\n'.encode())
        tnObj.read_until(en_prompt.encode(), timeout=5)
        if failed:
            chg = False
            msg = f'Device returned: unknown command {failed_cmds}'
            return (chg, msg)
        else:
            # tnObj.write('end'.encode() + '\n\n'.encode())
            # tnObj.read_until(en_prompt.encode(), timeout=5)
            chg = True
            msg = f"Configuration applied - {commands}"
    else:
        chg = False
        msg = "ERROR: Could not configure device"
    return (chg, msg)


def gns3_node_login(tnObj, user=None, pwd=None):
    """DOCSTRING."""
    chg = False
    curr_login, curr_prompt = get_mode(tnObj)
    if 'login' in curr_login:
        _r = get_in(tnObj, user, pwd)
        time.sleep(1)
        if _r[1] < 0:
            raise AnsibleError(f"Login failed, check {curr_login}")

    else:

        while curr_login is None:
            time.sleep(10)
            curr_login, curr_prompt = get_mode(tnObj)

    msg = curr_login
    return (chg, msg)


def gns3_boot_status(starting, tnObj):
    """DOCSTRING."""
    itime = time.time()

    booted = ''
    if starting:
        start_prompt = 'login'.encode()
    else:
        start_prompt = ''

    # repeat check of node boot status
    while not booted:
        booted, _b = get_mode(tnObj, start_prompt)

    delayed = (time.time() - itime) * 1000
    if delayed > 1000:
        delayed = f"{delayed / 1000:.2f} sec"
    else:
        delayed = f"{delayed:.2f}ms"

    booted = f"{delayed}"
    return (True, booted)


def check_cmd_params(module):
    """Determine if the commands parameter was provided.

    Next, verify the necessay supporting parameters.
    """
    cmds = False
    cmd_required_params = {
        'remote_dev': module.params['remote_dev'],
        'remote_prt': module.params['remote_prt'],
        'user': module.params['user'],
        'password': module.params['password'],
    }

    commands = module.params.get('commands')
    for cmd in commands:
        if cmd:
            cmds = True
            # test for the required parameters of commands
            for _param, _mod in cmd_required_params.items():
                if not _mod:
                    module.fail_json(msg=f"commands option missing, {_param}")
    return (True, commands) if cmds else (False, None)
