import logging

logging.basicConfig(level=logging.DEBUG, filename='netmods.log',
                    format='%(asctime)s %(levelname)-5s: %(message)s',
                    datefmt='%b %d %H:%M:%S')


def get_mode(connObj, prompt=None):
    """Determine network device access and priv mode.

    :param connObj: Telnet object to use once established with device
    :type connObj: telnetlib connection object
    :param prompt: The desired prompt
    :type prompt: string, optional

    :return: Tuple of priveledge mode and the prompt
    :rtype: tuple
    """
    if not prompt:
        prompts = [b'in\:', b'\)#', b'\>', b'\#', b'name\:']
    else:
        prompts = [prompt]

    # determine which 'mode' by prompts
    logging.info(
        f"[gm] [  IDx{str(hash(connObj))[0:6]}] verifying device mode")
    for i in range(1):
        connObj.write('\r'.encode())
        idx, obj, match = connObj.expect(prompts, timeout=5)

        _rx = match.splitlines()  # clean rec'd data into list
    _rx = _rx[-1]    # retreive last item in list
    logging.debug(f"[gm]  output recvd: {_rx}")

    if idx == 0:
        mode = 'login'   # login prompt
    elif idx == 1:
        mode = 'config'  # conf mode
    elif idx == 2:
        mode = 'priv_1'  # privilege 1
    elif idx == 3:
        mode = 'priv_15'  # privilege 15
    elif idx == 4:
        mode = 'user'  # username prompt
    else:
        mode = None
    logging.debug(
        f"[gm] [  IDx{str(hash(connObj))[0:6]}] returning mode, {mode}")
    return (mode, match.strip())  # return mode and prompt


def get_in(connobj, user=None, pw=None):
    """Login function to access device using telnetlib.

    :param connObj: Telnet object to use once established with device
    :type connObj: telnetlib connection object
    :param user: Username
    :type user: str
    :param pw: Password
    :type pw: str

    :return: True/False, idx, match
    :rtype: Tuple
    """
    connobj.write('\r\n'.encode())
    _r = connobj.read_until('-_$%_-'.encode(), timeout=1)
    if _r.decode().find(':') >= 1:
        _prompts = [b'\>', b'\#']
        # _prompts = [b'___$#', b'__(&^)']
        connobj.write(user.encode() + '\n'.encode())
        connobj.read_until('assword'.encode(), timeout=2)
        connobj.write(pw.encode() + '\n'.encode())
        idx, obj, match = connobj.expect(_prompts, timeout=5)

    msg = f"login successfull {idx}" if idx >= 0 else "login failed"
    logging.info(f"[gi] [  IDx{str(hash(connobj))[0:6]}] {msg}")
    return (True, idx, obj, match)


def get_enable(connobj, cmd='enable', user=None, pw=None):
    """Check if device is in enable mode, if not enter device into enable mode.

    :param connobj: Telnet object to use once established with device
    :type connobj: telnetlib connection object
    :param cmd: Device command to enter enable mode
    :type cmd: str
    :param user: Username of privledge user
    :type user: str
    :param pw: Password of privledge user to enter enable mode
    :type pwd: str

    :return: True or False
    :rtype: Boolean
    """
    cmd = cmd + '\n'
    pw = pw + '\n'
    enable_state = True

    logging.info("[en] verifying enable status")
    _r = get_mode(connobj)
    if _r != 'priv_15':
        # verify prompt
        logging.info("[en] device current mode, {}".format(_r[0]))
        connobj.write(cmd.encode())
        _p = connobj.read_until('assword: '.encode(), timeout=2)
        _rx = _p.decode().splitlines()  # clean rec'd data into list
        _rx = _rx[-1]    # prompt is last item in list
        logging.debug("[en] output recvd: {}".format(_rx))

        # check for return errors
        if '%' in _p.decode():
            logging.debug("cannot establish enable mode, {}".format(_p))
            enable_state = False
        else:
            logging.info("[en] sending password")
            connobj.write(pw.encode())
            _p = connobj.read_until('#'.encode(), timeout=2)
            mode = get_mode(connobj)[0]
            enable_state = True if mode == 'priv_15' else False
            logging.info("[en] enable mode is {}".format(enable_state))
    return enable_state


def get_in_confmode(connobj, hostname='',
                    cfg_prompt='(config)#', cmd='config t'):
    """Enter into configuration mode.

    :param connobj: Telnet object to use once established with device
    :type connobj: telnetlib connection object

    :return: True or False and configuration prompt
    :rtype: Tuple
    """
    cmd = cmd + '\n'
    conf_mode = True
    logging.info(f"[cf] [  IDx{str(hash(connobj))[0:6]}] Entering config mode")
    if hostname != '':
        cfg_prompt = '{}{}'.format(hostname, cfg_prompt)  # update prompt
        msg = 'hostname set to {}'.format(hostname)
    else:
        msg = 'hostname not set'
    logging.info(f"[cf] [  IDx{str(hash(connobj))[0:6]}] {msg}")

    logging.info(f"[cf] [  IDx{str(hash(connobj))[0:6]}] calling get_mode")
    _r = get_mode(connobj)
    if _r != 'config':
        connobj.write(cmd.encode())
        _r = connobj.read_until(cfg_prompt.encode(), timeout=2)
        logging.debug(
            f"[cf] [  IDx{str(hash(connobj))[0:6]}] recvd get_mode output: {_r}")

        _rx = _r.decode().splitlines()  # clean rec'd data into list
        _rx_prompt = _rx[-1]    # prompt is last item in list
        logging.info(f"[cf] derived config-mode prompt, {cfg_prompt}")

        if cfg_prompt not in _r.decode():
            logging.critical(
                f"[cf] [  IDx{str(hash(connobj))[0:6]}] failed to enter config mode")
            logging.critical(
                f"[cf] prompt recvd: {_rx_prompt}, expected: {cfg_prompt}")
            conf_mode = False
    logging.info(f"[cf] config mode is {conf_mode}")
    return (conf_mode, cfg_prompt)


def iou_rtr1(connObj, hostname='IOU1'):
    """Apply ssh configuration onto Cisco IOU router 1 only."""
    '''
    hostname <host>
    ip domain name <domain>
    crypto key generate rsa
    How many bits in the modulus [512]: 1024/2048
    ip ssh version 2
    !
    aaa new-model
    aaa authentication login default local
    aaa authorization exec default local
    !
    username <config_user> privilege 15 secret <password>
    !
    line vty 0 4
    transport input ssh
    !
    ! for testing only: no authentication on console
    aaa authentication login no_auth none
    line con 0
    privilege level 15
    login authentication no_auth
    '''

    device_ready = [b'\#', b'\>', b'\:']

    # obtain current prompt state on devices
    prompt_status = get_mode(connObj)[0]
    logging.debug("device current mode, {}".format(prompt_status))

    # enter into priv-15 mode
    if prompt_status is None:
        logging.info("sending ESC sequences to device")
        connObj.write('\x1B\r\n'.encode())  # send ESC and 2x carriage returns
        idx, obj, match = connObj.expect(device_ready, timeout=2)
        if idx >= 0:
            logging.info("device in ready state, matched: {}"
                         .format(device_ready[idx]))
            enable = get_enable(connObj, pw="Cisco")
        else:
            logging.critical("failed to determine device state")
            logging.critical("device returned, {}".format(match))
            logging.critical("cannot proceed with configuration")
            return False
            # raise SystemExit("Exiting script due to errors")
    elif prompt_status == 'priv_1':
        enable = get_enable(connObj, pw='Cisco')

    # enter into config mode
    if not enable:
        raise SystemExit("ERROR: Exiting due to error with enable mode")

    conf_mode = get_in_confmode(connObj)

    if True not in conf_mode:
        logging.critical("config mode return {}".format(conf_mode))
        raise SystemExit("ERROR: Exiting due to error in config mode")

    # setup ssh configuration
    cfg_prompt = conf_mode[1]
    feedback = [b"modulus [512]:", b"replace them"]

    hstname_cmd = "hostname {}".format(hostname)
    connObj.write(hstname_cmd.encode() + '\n\n'.encode())
    connObj.read_until(cfg_prompt.encode(), timeout=5)
    connObj.write('ip domain name lab.ioi'.encode() + '\n\n'.encode())
    connObj.read_until(cfg_prompt.encode(), timeout=5)
    connObj.write('crypto key generate rsa'.encode() + '\n'.encode())
    _idx, _obj, _match = connObj.expect(feedback, timeout=5)
    if _idx == 1:
        logging.info("RSA keys already defined. Replacing")
        connObj.write('yes'.encode() + '\n'.encode())
    logging.info("Entering module size of 1024")
    connObj.write('1024'.encode() + '\n'.encode())
    connObj.write('ip ssh version 2'.encode() + '\n\n'.encode())
    connObj.read_until(cfg_prompt.encode(), timeout=5)
    connObj.write('aaa new-model'.encode() + '\n'.encode())
    logging.info("Applying AAA configuration")
    connObj.read_until(cfg_prompt.encode(), timeout=5)
    connObj.write(
        'aaa authentication login default local'.encode() + '\n\n'.encode())
    connObj.write(
        'aaa authorization exec default local'.encode() + '\n\n'.encode())
    connObj.write(
        'username admin privilege 15 secret Cisco'.encode() + '\n\n'.encode())
    connObj.read_until(cfg_prompt.encode(), timeout=5)
    connObj.write('line vty 0 4'.encode() + '\n\n'.encode())
    connObj.write('transport input ssh'.encode() + '\n\n'.encode())
    connObj.read_until(cfg_prompt.encode(), timeout=5)
    connObj.write('end'.encode() + '\n'.encode())
    if 'enable' not in get_mode(connObj):
        logging.debug("[iou] device is trailing, buffering")
        connObj.read_until('-_$%_-'.encode(), timeout=2)
        connObj.write('\r\n'.encode() + '\n'.encode())
        state = connObj.read_until('-_$%_-'.encode(), timeout=1)
        connObj.write('end'.encode() + '\n'.encode())
    return True

    '''
    # tn = telnetlib.Telnet('192.168.1.140', 23, timeout=2)
    if get_mode(tn)[0] == 'user':
        # print("Entering username")
        tn.write('admin\n'.encode())
        tn.read_until('assword:'.encode(), timeout=2)
        tn.write('Cisco\n'.encode())
        tn.read_until('>'.encode(), timeout=2)
        tn.write('sh ver\n'.encode())
        r = tn.read_until('>'.encode(), timeout=2)
        print(r.decode().find('Arista'))


except Exception as e:
    raise SystemExit(
        '\nERROR: Telnet failed to device on port 23, {}'.format(e))
'''
